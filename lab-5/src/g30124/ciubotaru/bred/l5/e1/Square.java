package g30124.ciubotaru.bred.l5.e1;

public class Square extends Rectangle {

    protected double side;
    public void Square(){};

    public void Square(double side)
    {
        this.side = side;
    }

    public void Square(double side, String color, boolean filled){
        this.side = side;
        this.color = color;
        this.filled = filled;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public void setWidth(double side) {
        this.side = side;
    }

    @Override
    public double getLength(double side) {
        return this.side = side;
    }

    @Override
    public String toString() {
        return "Square{"+"color="+color+"\\"+", filled = "+filled+", length="+getLength()+", width="+getWidth()+"}" ;
    }
}
