package g30124.ciubotaru.bred.l5.e1;

public class Circle extends Shape {

    protected double radius;

    public Circle(){};
    public Circle(double radius){
      this.radius = radius;
    };
    public Circle(String color, boolean filled, double radius)
    {
        super(color,filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI*Math.PI*getRadius();
    }

    @Override
    public double getPerimeter() {
        return 2*Math.PI*getRadius();
    }

    @Override
    public String toString() {
        return "Circle{"+"color="+color+"\\"+", filled = "+filled+", radius="+getRadius()+"}" ;
    }
}
