package g30124.ciubotaru.bred.l5.e1;

public class Rectangle extends Shape{

    protected double width;
    protected double length;

    public void Rectangle(){};
    public void Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }

    public void Rectangle(double width, double length, String color, boolean filled )
    {
        this.width = width;
        this.length = length;
        this.color = color;
        this.filled = filled;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return getWidth()*getLength();
    }

    @Override
    public double getPerimeter() {
        return 2*(getWidth()+getLength());
    }

    @Override
    public String toString() {
        return "Rectangle{"+"color="+color+"\\"+", filled = "+filled+", length="+getLength()+", width="+getWidth()+"}" ;
    }

}
