package g30124.ciubotaru.bred.l5.e2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private String imageType;

    public ProxyImage(String fileName,String imageType){
        this.fileName = fileName;
        this.imageType = imageType;
    }

    @Override
    public void display() {
        if(imageType == "real"){
           if (realImage == null) realImage = new RealImage(fileName);
            realImage.display();
        }
        else if(imageType == "rotated"){
            if (rotatedImage == null) rotatedImage = new RotatedImage(fileName);
            rotatedImage.display();
        }


    }
}
