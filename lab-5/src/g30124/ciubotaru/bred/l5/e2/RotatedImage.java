package g30124.ciubotaru.bred.l5.e2;

public class RotatedImage implements Image {

    private String fileName;

    public RotatedImage(String fileName){
        this.fileName = fileName;
        loadFromDisk(fileName);
    }

    public String getfileName() {
        return fileName;
    }

    public void setfileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void display() {
        System.out.println("Displaying rotated " + fileName);
    }

    private void loadFromDisk(String fileName)
    {
        System.out.println("Loading " + fileName);
    }
}
