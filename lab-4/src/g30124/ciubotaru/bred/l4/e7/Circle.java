package g30124.ciubotaru.bred.l4.e7;

public class Circle {

    private double radius;
    private String color;

    Circle()
    {
        radius = 1.00;
        color = "red";
    }

    Circle(double radius)
    {
        this.radius = radius;
        this.color = "red";
    }

    public double getRadius() {
        return radius;
    }

    public double getArea()
    {
        return Math.PI*Math.PI*radius;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
