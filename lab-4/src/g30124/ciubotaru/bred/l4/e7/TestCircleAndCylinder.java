package g30124.ciubotaru.bred.l4.e7;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestCircleAndCylinder {

    @Test
    public void testCylinder()
    {
        Cylinder newCyl = new Cylinder(2.0,5.0);
        assertEquals(5.0, newCyl.getHeight() );
        assertEquals(2.0,newCyl.getRadius());
        assertEquals(2.0*5.0*2.0*Math.PI,newCyl.getVolume());
    }
}
