package g30124.ciubotaru.bred.l4.e7;

public class Cylinder extends Circle {
    private double height ;
    Cylinder()
    {
        super();
        height = 1.0;
    }

    Cylinder(double radius)
    {
        super(radius);
    }
    Cylinder(double radius, double height)
    {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume()
    {
        return this.getHeight()*super.getRadius()*2*Math.PI;
    }
}
