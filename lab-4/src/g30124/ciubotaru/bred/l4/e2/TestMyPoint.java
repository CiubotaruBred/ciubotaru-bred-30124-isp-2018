package g30124.ciubotaru.bred.l4.e2;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestMyPoint {


   @Test
    public void createPointAndGetX()
   {
       MyPoint point = new MyPoint(2,5);
       assertEquals(2, point.getX());
   }
    @Test
    public void createPointAndGetY()
    {
        MyPoint point = new MyPoint(2,5);
        assertEquals(2, point.getX());
    }

    @Test
    public void createPointAndSetX()
    {
        MyPoint point = new MyPoint(2,5);
        point.setX(5);
        assertEquals(5, point.getX());
    }


    @Test
    public void createPointAndGSetY()
    {
        MyPoint point = new MyPoint(2,5);
        point.setY(5);
        assertEquals(5, point.getY());
    }
}