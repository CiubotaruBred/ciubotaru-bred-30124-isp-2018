package g30124.ciubotaru.bred.l4.e5;

import g30124.ciubotaru.bred.l4.e4.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock;

    Book()
    {
        this.qtyInStock = 0;
    }
    Book(String name, Author author, double price)
    {
        this.name = name;
        this.author = author;
        this.price = price ;
    }

    Book (String name, Author author, double price,
          int qtyInStock)
    {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return getName()+" by "+getAuthor().toString();
    }
}
