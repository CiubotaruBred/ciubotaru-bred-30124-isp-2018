package g30124.ciubotaru.bred.l4.e6;

import g30124.ciubotaru.bred.l4.e4.Author;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestBook2 {

    @Test
    public void testGetAndSetPrice()
    {
        Author Ion = new Author("Ion Creanga", "IonCreanga@yahoo.com",'m');
        Author Ion2= new Author("Ion Creangax", "IonCreangax@yahoo.com",'m');
        Author[] theTwo = new Author[2];
        theTwo[0] = Ion;
        theTwo[1] = Ion2;


        Book newBook = new Book("Harap Alb",theTwo,12.5);
        newBook.setPrice(125);
        assertEquals(125.0,newBook.getPrice());
    }

    @Test
    public void testVolume()
    {
        Author Ion = new Author("Ion Creanga", "IonCreanga@yahoo.com",'m');
        Author Ion2= new Author("Ion Creangax", "IonCreangax@yahoo.com",'m');
        Author[] theTwo = new Author[2];
        theTwo[0] = Ion;
        theTwo[1] = Ion2;
        Book newBook = new Book("Harap Alb",theTwo,12.5,100);
        newBook.setQtyInStock(500);
        assertEquals(500,newBook.getQtyInStock());
    }


}
