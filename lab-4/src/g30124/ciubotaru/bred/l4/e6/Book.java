package g30124.ciubotaru.bred.l4.e6;

import g30124.ciubotaru.bred.l4.e4.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    Book()
    {
        this.qtyInStock = 0;
    }
    Book(String name, Author[] authors, double price)
    {
        this.name = name;
        this.authors = authors;
        this.price = price ;
    }

    Book(String name, Author[] authors, double price,
         int qtyInStock)
    {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public int getAuthorNumber()
    {
        return getAuthors().length;
    }
    @Override
    public String toString() {
       return getName() + " by " + getAuthorNumber() + " authors";
    }
    public void printAuthors()
    {
        for (int i = 0;i<this.authors.length;i++)
        {
            System.out.println(authors[i].getName()+" ");
        }
    }
}
