package g30124.ciubotaru.bred.l4.e3;

public class Circle {
    private double radius;
    private String color;
    Circle()
    {
        this.radius = 1.0;
        this.color = "red";
    }
    Circle(double radius)
    {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return getRadius()*Math.PI*Math.PI;
    }
}
