package g30124.ciubotaru.bred.l4.e3;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestCircle {
    @Test
    public void testRadius()
    {
        Circle s = new Circle(5);
        assertEquals(5.0, s.getRadius());
    }

    @Test
    public void testArea()
    {
        Circle s = new Circle(5);
        assertEquals(5.0*Math.PI*Math.PI, s.getArea());
    }
}
