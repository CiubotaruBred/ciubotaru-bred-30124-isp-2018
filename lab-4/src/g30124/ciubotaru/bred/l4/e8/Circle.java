package g30124.ciubotaru.bred.l4.e8;

public class Circle extends Shape{

    private double radius;
    Circle()
    {
        super();
        this.radius = 1.0;
    }

    Circle(double radius)
    {
        super();
        this.radius = radius;
    }

    Circle(double radius, String color, boolean filled)
    {
        super(color,filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea()
    {
        return getRadius()*Math.pow(Math.PI,2);
    }

    public double getPerimeter()
    {
        return getRadius()*2.0*Math.PI;
    }

    @Override
    public String toString() {
        return "A Circle with radius= "+getRadius()+", which is a subclass of " +super.toString();

    }
}
