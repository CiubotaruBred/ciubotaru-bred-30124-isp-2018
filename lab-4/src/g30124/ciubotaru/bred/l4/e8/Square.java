package g30124.ciubotaru.bred.l4.e8;

public class Square extends Rectangle{
    Square()
    {
        super();
    }

    Square(double side)
    {
        super(side,side);
    }

    Square(double side, String color, boolean filled)
    {
        super(side,side,color,filled);
    }

    public double getSide()
    {
        return getWidth();
    }

    public void setSide(double side)
    {
        setWidth(side);
        setLength(side);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        super.setLength(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
        super.setWidth(length);
    }

    @Override
    public String toString() {
        return "A Square with side= "+getSide()+", which is a subclass of "+  super.toString();
    }
}
