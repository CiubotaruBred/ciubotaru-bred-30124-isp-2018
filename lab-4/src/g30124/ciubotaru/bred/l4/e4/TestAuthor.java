package g30124.ciubotaru.bred.l4.e4;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestAuthor {

    @Test
    public void testGetAndSetEmail(){
        Author mark = new Author("Mark","Twain@yahoo.com",'m');
        mark.setEmail("Twain@gmail.com");
        assertEquals("Twain@gmail.com", mark.getEmail());
    }

    @Test
    public void testToString(){

        Author joan = new Author("John","Wick@yahoo.com",'m');
        assertEquals("John(m)@Wick@yahoo.com",joan.toString());
    }

    @Test
    public void testGetName(){
        Author joan = new Author("John","Wick@yahoo.com",'m');
        assertEquals("John",joan.getName());

    }
}
