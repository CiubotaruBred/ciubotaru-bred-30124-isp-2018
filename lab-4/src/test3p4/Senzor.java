package test3p4;

import org.junit.Test;

import java.util.Random;

import static junit.framework.Assert.assertEquals;

public class Senzor {

    private Random rand = new Random();
    private int value = rand.nextInt(2);

    public static void main(String[] args)
    {
        Controller ctrl = new Controller();
        System.out.println(ctrl.senz[0].read());;
        System.out.println(ctrl.senz[1].read());
        System.out.println(ctrl.control());
    }

    public int read()
    {
        return this.value;
    }

    @Test
    public void testController()
    {
        Controller ctrl = new Controller();
        //assertEquals(5,ctrl.senz[0].read());
        //assertEquals(7,ctrl.senz[1].read());
        assertEquals(true, ctrl.senz[0] instanceof SenzorTemperatura);
        assertEquals(true, ctrl.senz[1] instanceof SenzorUmiditate);
        //assertEquals( 0, ctrl.control());
    }

    public static class SenzorTemperatura extends Senzor{
        private Random rand = new Random();
        private int temperatura = rand.nextInt(100);
        public int read()
        {
            return this.temperatura;
        }
    }

    public static class SenzorUmiditate extends Senzor{
        private Random rand = new Random();
        private int umiditate = rand.nextInt(150);
        public int read()
        {
            return this.umiditate;
        }
    }

    public static class Controller
    {
        static Senzor[] senz = new Senzor[2];
        public Controller()
        {
            senz[0] = new SenzorTemperatura();
            senz[1] = new SenzorUmiditate();
        }

        public int control()
        {
            if (senz[0].read()>40 || senz[1].read()>70) return 0;
            else return -1;
        }
    }
}

