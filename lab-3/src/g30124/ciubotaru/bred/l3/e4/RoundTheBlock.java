package g30124.ciubotaru.bred.l3.e4;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;
public class RoundTheBlock {


    public static void main(String[] args)
    {
        City cj = new City();
        Wall block1 = new Wall(cj,1,1,Direction.WEST);
        Wall block2 = new Wall(cj,2,1,Direction.WEST);
        Wall block3 = new Wall(cj,2,1,Direction.SOUTH);
        Wall block4 = new Wall(cj,2,2,Direction.SOUTH);
        Wall block5 = new Wall(cj,2,2,Direction.EAST);
        Wall block6 = new Wall(cj,1,2,Direction.EAST);
        Wall block7 = new Wall(cj,1,2,Direction.NORTH);
        Wall block8 = new Wall(cj,1,1,Direction.NORTH);

        Robot karel = new Robot(cj,0,2,Direction.WEST);
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.move();
        karel.move();
        karel.turnLeft();
        karel.move();
// la ex 7 se cer getter si settere

    }


}
