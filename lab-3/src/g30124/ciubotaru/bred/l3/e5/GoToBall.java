package g30124.ciubotaru.bred.l3.e5;

import becker.robots.*;


public class GoToBall {

    public static void main(String[] args)
    {
        City cj = new City();
        Wall block4 = new Wall(cj,1,2,Direction.SOUTH);
        Wall block6 = new Wall(cj,1,2,Direction.EAST);
        Wall block7 = new Wall(cj,1,2,Direction.NORTH);
        Wall block8 = new Wall(cj,1,1,Direction.NORTH);
        Wall block1 = new Wall(cj,1,1, Direction.WEST);
        Wall block2 = new Wall(cj,2,1,Direction.WEST);
        Wall block3 = new Wall(cj,2,1,Direction.SOUTH);

        Robot karel = new Robot(cj,1,2,Direction.SOUTH);
        Thing parcel = new Thing(cj, 2, 2);

        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.move();
        karel.pickThing();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();
        karel.move();
        karel.turnLeft();
        karel.turnLeft();
        karel.turnLeft();

    }
}
