package g30124.ciubotaru.bred.l3.e6;

import java.text.DecimalFormat;

public class TestMyPoint {


    public static void main(String[] args) {
        MyPoint point1 = new MyPoint(1,2);
        MyPoint point2 = new MyPoint(5,7);
        System.out.println("Point1 is at: "+point1.toString());
        System.out.println("Point2 is at: "+point2.toString());
        //System.out.println("point1 - x = " + point1.getX() + " y = " + point1.getY());
        System.out.println("Distance from point1 to (15,2) is: "+point1.distance(15,2));
        double number = point1.distance(point2);
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(3);
        System.out.println("Distance from point1 to point2 is: "+df.format(number));
    }
}