package algo1p1;

public class Solution {
    public static int solution(int[] A, int N){
        int i,j,k, found = 0;
        int[] passed = new int[N];
        for (i = 0;i<N;i++) passed[i] = 0;
        for (i=0;i<N-1;i++) {
            k = 1;
            if (passed[i] == 1) continue;
            for (j = i+1;j<N;j++)
            {
                if ( (A[i] == A[j]))
                {
                    passed[j] = 1;
                    k = 0;
                }
            }
            if (k == 1) found = A[i];
        }

        return found;
    }


    public static void main(String[] args){
        int[] a = new int[7];
        a[0] = 9; a[1] = 3; a[2] = 9; a[3] = 3; a[4] = 9; a[5] = 7; a[6] = 9;
        int x = solution(a,7);
        System.out.println(x);
    }

}

