package g30124.ciubotaru.bred.l6.e4;

public class CharSecvImpelementation implements CharSequence {

    private String in;
    @Override
    public int length() {
        return in.length();
    }

    @Override
    public char charAt(int index) {
        return in.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        StringBuilder s = new StringBuilder(in.subSequence(start,end));
        return s;
    }
}
