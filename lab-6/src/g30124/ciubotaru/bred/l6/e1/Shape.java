package g30124.ciubotaru.bred.l6.e1;

import java.awt.*;

public interface Shape {

    public String getId();
    public void setId(String id) ;

    public Color getColor();
    public void setColor(Color color);
    public abstract void draw(Graphics g);
}
