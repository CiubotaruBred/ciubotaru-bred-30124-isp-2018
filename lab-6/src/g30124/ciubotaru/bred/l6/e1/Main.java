package g30124.ciubotaru.bred.l6.e1;

import java.awt.*;
import java.util.Scanner;

import static java.lang.Boolean.TRUE;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Scanner in = new Scanner(System.in);
        Shape s1 = new Circle(Color.RED, 90);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE, 100, 250);
        System.out.println("Where do you want to place the shape? \n ");
        System.out.println("x=");
        int x;
        x = in.nextInt();
        System.out.println("y=");
        int y;
        y = in.nextInt();
        ((Rectangle)s3).setFilled(TRUE);
        ((Rectangle)s3).setX(x);
        ((Rectangle)s3).setY(y);
        b1.addShape(s3);

        in.nextLine();
        System.out.println("Introduce shape id");
        String shapeId = in.nextLine();
        in.close();
        s1.setId(shapeId);
        b1.deleteById(shapeId);
        s2.setId(shapeId);
        b1.deleteById(shapeId);

    }
}
