package g30124.ciubotaru.bred.l6.e1;


import java.awt.*;


public class Rectangle implements Shape{

    boolean filled;
    private int length;
    private int width;
    private int height;
    private int x;
    private int y;
    private Color color = null;
    private String id = null;

    public Rectangle(Color color, int length) {
        this.color = color;
        this.length = length;
    }

    public Rectangle (Color color, int width, int height)
    {
        this.color = color;
        this.width = width;
        this.height = height;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void draw(Graphics g) {

        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        g.setColor(getColor());
        if (isFilled()) g.fillRect(getX(),getY(),getWidth(),getHeight());
        g.drawRect(getX(),getY(),getWidth(),getHeight());
    }
}
