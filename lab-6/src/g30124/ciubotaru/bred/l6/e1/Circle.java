package g30124.ciubotaru.bred.l6.e1;


import java.awt.*;

public class Circle implements Shape{

    private int radius;
    private Color color = null;
    private String id = null;

    public Circle(Color color, int radius) {
        this.color = color;
        this.radius = radius;
    }

    public Circle(Color color) {
        this.color = color;
    }

    public int getRadius() {
        return this.radius;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
        g.setColor(getColor());
        g.drawOval(50,50,radius,radius);
    }
}
