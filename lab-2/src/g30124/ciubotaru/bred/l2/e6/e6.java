package g30124.ciubotaru.bred.l2.e6;

import java.util.Scanner;

public class e6 {


    private static int factorialNerec(int n) {
        int i, prod = 1;
        for (i = 1; i <= n; i++) prod *= i;
        return prod;
    }


    private static int factorialRec(int n) {
        if (n == 0) return 1;
        else return n * factorialRec(n - 1);
    }

    public static void main(String[] args) {
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("n=");
        n = in.nextInt();
        in.close();
        System.out.println("Unrecursive factorial is: " + factorialNerec(n));
        System.out.println("Recursive factorial is: " + factorialRec(n));

    }

}
