package g30124.ciubotaru.bred.l2.e4;

import java.util.Scanner;

public class e4 {

    /*static int maxim (int n, int v[]) {
		{
		  int max=v[0];
		  for(int i=0;i<n;i++)
		        if(max<v[i])
		            max=v[i];
		  return max;
		 }

	}*/
    public static void main(String[] args) {
        int nr, i;
        int max = -999999;
        Scanner in = new Scanner(System.in);
        System.out.print("n=");
        nr = in.nextInt();

        int[] vect = new int[nr];
        for (i = 0; i < nr; i++) {
            System.out.print("vect[" + i + "]=");
            vect[i] = in.nextInt();
            if (max < vect[i]) max = vect[i];
        }
        in.close();
        System.out.println("Vector maximum is= " + max);
    }
}
