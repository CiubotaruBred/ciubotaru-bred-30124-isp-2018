package g30124.ciubotaru.bred.l2.e1;

import java.util.Scanner;

public class e1 {

    public static void main(String[] args) {
        int n1, n2, max;
        Scanner in = new Scanner(System.in);
        System.out.print("n1=");
        n1 = in.nextInt();
        System.out.print("n2=");
        n2 = in.nextInt();
        in.close();
        if (n1 > n2)
            max = n1;
        else
            max = n2;
        System.out.println("Maximul este: " + max);
    }
}

