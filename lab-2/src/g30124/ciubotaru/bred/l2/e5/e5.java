package g30124.ciubotaru.bred.l2.e5;

import java.util.Scanner;

public class e5 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] a = new int[10];
        int i;
        for (i = 0; i < 10; i++) {
            System.out.print("a[" + i + "]=");
            a[i] = in.nextInt();
        }

        int k = 0;
        int aux;
        while (k == 0) {
            k = 1;
            for (i = 0; i < 9; i++) {
                if (a[i] > a[i + 1]) {
                    aux = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = aux;
                    k = 0;
                }
            }
        }
        in.close();
        for (i = 0; i < 10; i++) {
            System.out.print(a[i] + " ");
        }
    }
}
