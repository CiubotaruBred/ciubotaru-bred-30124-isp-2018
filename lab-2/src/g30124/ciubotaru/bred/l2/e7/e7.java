package g30124.ciubotaru.bred.l2.e7;

import java.util.Random;
import java.util.Scanner;

public class e7 {

    public static void main(String[] args) {
        Random n = new Random();
        Scanner in = new Scanner(System.in);
        int guess = n.nextInt(100);

        int userGuess = 0;
        int noOfGuesses = 0;

        while (noOfGuesses < 3 && userGuess != guess) {
            System.out.print("Guess the number:");
            userGuess = in.nextInt();
            if (userGuess < guess) {
                System.out.println("Too low");
                noOfGuesses++;
            } else if (userGuess > guess) {
                System.out.println("Too high");
                noOfGuesses++;
            }
        }
        if (noOfGuesses < 3) System.out.println("Your guess is correct! Good Job!");
        else System.out.println("You failed!");
        System.out.println("The number was: " + guess);
        in.close();

    }
}
