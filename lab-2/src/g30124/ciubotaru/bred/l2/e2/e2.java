package g30124.ciubotaru.bred.l2.e2;

import java.util.Scanner;

public class e2 {


    private static void ifVersion(int a) {
        String number;
        if (a == 0) number = "ZERO";
        else if (a == 1) number = "ONE";
        else if (a == 2) number = "TWO";
        else if (a == 3) number = "THREE";
        else if (a == 4) number = "FOUR";
        else if (a == 5) number = "FIVE";
        else if (a == 6) number = "SIX";
        else if (a == 7) number = "SEVEN";
        else if (a == 8) number = "EIGHT";
        else if (a == 9) number = "NINE";
        else number = "OTHER";
        System.out.println(number);
    }


    private static void switchVersion(int b) {
        String number;
        switch (b) {
            case 0:
                number = "ZERO";
                break;
            case 1:
                number = "ONE";
                break;
            case 2:
                number = "TWO";
                break;
            case 3:
                number = "THREE";
                break;
            case 4:
                number = "FOUR";
                break;
            case 5:
                number = "FIVE";
                break;
            case 6:
                number = "SIX";
                break;
            case 7:
                number = "SEVEN";
                break;
            case 8:
                number = "EIGHT";
                break;
            case 9:
                number = "NINE";
                break;
            default:
                number = "OTHER";
                break;
        }
        System.out.println(number);
    }


    public static void main(String[] args) {
        int n;
        Scanner in = new Scanner(System.in);
        System.out.print("n=");
        n = in.nextInt();
        in.close();

        System.out.println("Nested if: ");
        ifVersion(n);

        System.out.println("Switch version:");
        switchVersion(n);

    }
}
