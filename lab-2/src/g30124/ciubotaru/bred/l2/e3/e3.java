package g30124.ciubotaru.bred.l2.e3;

import java.util.Scanner;

public class e3 {
    private static boolean prim(int n) {
        int i, s = 0;
        for (i = 1; i <= n; i++)
            if (n % i == 0)
                s++;
        return s == 2;
    }

    public static void main(String[] args) {
        int A, B, i, nr = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("A = ");
        A = in.nextInt();
        System.out.print("B =");
        B = in.nextInt();
        in.close();

        for (i = A; i <= B; i++)
            if (prim(i)) {
                System.out.print(i + " ");
                nr++;
            }
        System.out.println();
        System.out.println("Prime number count = " + nr);
    }
}
