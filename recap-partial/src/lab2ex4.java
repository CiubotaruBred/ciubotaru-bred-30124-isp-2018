import java.util.Arrays;
import java.util.Scanner;

/*
Giving a vector of N elements, display the maximum element in the vector.
 */
public class lab2ex4 {

    private static int N;
    private static int[] A;


    public static int getN() {
        return N;
    }

    public static void setN(int n) {
        N = n;
    }

    public static int[] getA() {
        return A;
    }

    public static void setA(int[] a) {
        A = a;
    }

    public static int getMax(int[] a)
    {
        int i;
        int max = a[0];
        for (i = 0;i<a.length;i++)
        {
            if (a[i] > max) max = a[i];
        }
        return max;
    }

    public static void main(String[] args)
    {

        Scanner in = new Scanner(System.in);
        System.out.println("N=");
        setN(in.nextInt());

        int[] a = new int[getN()];
        int i ;
        for (i = 0;i<getN();i++)
        {
            System.out.println("a["+i+"]=");
            a[i] = in.nextInt();
        }
        setA(a);
        System.out.println("Vectorul este:");
        System.out.println(Arrays.toString(getA()));
        System.out.println("Maximul vectorului este:");
        System.out.println(getMax(getA()));

    }
}
