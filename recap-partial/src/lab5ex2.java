//interface Image {
//    void display();
//}
//
//class RealImage implements Image {
//
//    private String fileName;
//
//    public RealImage(String fileName){
//        this.fileName = fileName;
//        loadFromDisk(fileName);
//    }
//
//    @Override
//    public void display() {
//        System.out.println("Displaying " + fileName);
//    }
//
//    private void loadFromDisk(String fileName){
//        System.out.println("Loading " + fileName);
//    }
//}
//
//class ProxyImage implements Image{
//
//    private RealImage realImage;
//    private String type;
//    private RotatedImage rotatedImage;
//    private String fileName;
//
//    public ProxyImage(String fileName, String type){
//        this.fileName = fileName;
//        this.type = type;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    @Override
//    public void display() {
//        if (getType() == "real") {
//            if (realImage == null) {
//                realImage = new RealImage(fileName);
//            }
//            realImage.display();
//        } else if (getType() == "rotated")
//        {
//            if (rotatedImage == null) {
//                rotatedImage= new RotatedImage(fileName);
//            }
//            rotatedImage.display();
//        }
//    }
//}
//
//class RotatedImage implements Image {
//
//    private String fileName;
//
//    public RotatedImage(String fileName) {
//        this.fileName = fileName;
//    }
//
//    @Override
//    public void display()
//    {
//        System.out.println("Display rotated "+filename);
//    }
//}