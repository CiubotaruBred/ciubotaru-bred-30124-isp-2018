
public class MyPoint{
    private int x,y;
    //static int sexyTime;
    public MyPoint()
    {
        x = 0;
        y = 0;
    }
    public MyPoint(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x, int y)
    {
        setX(x);
        setY(y);
    }

    @Override
    public String toString() {
        return "("+getX()+","+getY()+")";
    }

    public double distance(int x,int y)
    {
        return Math.sqrt(Math.pow(getX()-x,2)+Math.pow(getY()-y,2));
    }
    public double distance(MyPoint another)
    {
        return Math.sqrt(Math.pow(getX()-another.getX(),2)+Math.pow(getY()-another.getY(),2));
    };
}
