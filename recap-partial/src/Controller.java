import java.util.Random;

public class Controller {

    private LightSensor lightSensor;
    private TemperatureSensor temperatureSensor;

//    public LightSensor getLightSensor() {
//        return lightSensor;
//    }
//
//    public int setLightSensorValue(LightSensor lightSensor) {
//        this.lightSensor.value  = lightSensor;
//    }
//
//    public TemperatureSensor getTemperatureSensor() {
//        return temperatureSensor;
//    }
//
//    public void setTemperatureSensorValue(TemperatureSensor temperatureSensor) {
//        this.temperatureSensor = temperatureSensor;
//    }

     private Controller(){
        lightSensor = new LightSensor();
        temperatureSensor = new TemperatureSensor();
    }

    public LightSensor getLightSensor() {
        return lightSensor;
    }

    public TemperatureSensor getTemperatureSensor() {
        return temperatureSensor;
    }

    public void control() throws InterruptedException {
        int time = 0;
        Random x = new Random();
        while (time<20)
        {   lightSensor = new LightSensor();
            temperatureSensor = new TemperatureSensor();
            System.out.println("Light is: "+getLightSensor().readValue());
            System.out.println();
            System.out.println("Temperature is "+getTemperatureSensor().readValue());
            Thread.sleep(1000);
            time++;
//            setLightSensor.s(x.nextInt());
//            setTemperatureSensor(x.nextInt());

        }
    }
}
