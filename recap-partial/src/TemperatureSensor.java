import java.util.Random;

public class TemperatureSensor extends Sensor {

    private int tempValue;
    public TemperatureSensor()
    {
        Random x = new Random();
        this.tempValue = x.nextInt(100);
    }

    @Override
    public int readValue() {
        return tempValue;
    }
}
