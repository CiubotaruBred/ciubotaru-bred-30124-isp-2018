import java.util.Random;

public class LightSensor extends Sensor{


    private int tempValue;

    public LightSensor() {
        Random x = new Random();
        this.tempValue = x.nextInt(100);
    }

    @Override
    public int readValue() {
        return tempValue;
    }
}

