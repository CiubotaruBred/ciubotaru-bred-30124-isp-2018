/*
Write a program which display prime numbers between A and B, where A and B are read from console.
 Display also how many prime numbers have been found.
 */

import java.util.Scanner;

public class lab2ex3 {

    private static int A;
    private static int B;

    public static int getA() {
        return A;
    }

    public static void setA(int a) {
        A = a;
    }

    public static int getB() {
        return B;
    }

    public static void setB(int b) {
        B = b;
    }

    public static boolean checkInterval(int a,int b)
    {
        if (a<b) return true;
        else return false;
    }
    public static boolean isPrime(int counter)
    {
        int primeCounter;
        if (counter < 2) return false;
        for (primeCounter = 2; primeCounter<=Math.sqrt(counter);primeCounter++)
        {

            if (counter%primeCounter == 0) return false;
        }
        return true;
    }
    public static void main(String[] args)
    {
        int counter, primeCounter,a, b, howManyPrimes = 0;
        boolean isPrime;
        boolean isValidInterval = false;
        while (isValidInterval == false) {
            Scanner in = new Scanner(System.in);
            System.out.println("A= ");
            a = in.nextInt();
            System.out.println("B= ");
            b = in.nextInt();
            setA(a);
            setB(b);
            if (checkInterval(A,B)) isValidInterval = true;
            else System.out.println("Interval gresit, alegeti A < B");
        }
        System.out.println("Lista de numere prime este: \n");
        for (counter = getA(); counter<=getB(); counter++  )
        {

            if (isPrime(counter))
            {
                System.out.println(counter+" ");
                howManyPrimes++;
            }

        }
        System.out.println("Sunt "+howManyPrimes+" numere prime in intervalul ["+getA()+","+getB()+"]");
    }


}
